<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %><%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 16/01/2019
  Time: 3:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Exercise 1 - part 1</title>
    </head>
    <body>
        <h1>Lorem Ipsum</h1>

        <h2>Paragraphs </h2>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras blandit pretium diam, vitae dignissim ex finibus id. Praesent odio nisl, dapibus quis sagittis eu, sollicitudin eget purus. Nullam sem est, tincidunt id ex at, tincidunt dapibus velit. Morbi pulvinar sem ac massa fringilla, ut eleifend est volutpat. Fusce fermentum feugiat tellus, posuere tincidunt sem feugiat ac. Ut faucibus cursus neque ac hendrerit. Vestibulum eu tortor eget nisi porta pellentesque eget sagittis dolor. Praesent interdum tempus nibh, a laoreet arcu bibendum at. Nunc faucibus nunc vel consequat auctor. Mauris ut condimentum lectus. Suspendisse gravida aliquam turpis, et malesuada metus fermentum et. </p>

        <hr>

        <%--Creating a clock below--%>


        <%--initialise variables--%>
        <%! LocalDateTime time = LocalDateTime.now(); %>
        <%! DateTimeFormatter formatted = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"); %>

        <%! String timeString = time.format(formatted); %>

        <p> Date and Time (Current)

            <%= timeString %>
        </p>


    </body>
</html>
