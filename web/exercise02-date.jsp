<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %><%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 16/01/2019
  Time: 3:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%! LocalDateTime time = LocalDateTime.now(); %>
<%! DateTimeFormatter formatted = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"); %>

<%! String timeString = time.format(formatted); %>

<p> Date and Time (Current)

    <%= timeString %>
</p>
